import 'package:flutter/material.dart';
import 'Page/Page.dart';
import 'Page/NumberScreen.dart';
import 'Page/pageWebView.dart';

void main() {
  runApp(MaterialApp(
    home: HomePage(),
    routes: <String, WidgetBuilder>{
      "/PageIconCard": (BuildContext context) => CardIcon(),
      "/PageNumberScreen": (BuildContext context) =>
          NumberScreen(title: "Page Number Screen"),
      "/PageWebView": (BuildContext context) => PageWebView(),
    },
  ));
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,
          title: const Text('AppBar'),
          leading: Icon(Icons.home),
          actions: <Widget>[
            Icon(Icons.search),
            Icon(Icons.alarm),
          ],
        ),
        body: Container(
          width: 300,
          child: ListView(
            children: [
              Column(
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(20.0)),
                  Center(child: Text("Niomic")),
                  Padding(padding: EdgeInsets.all(20.0)),
                  Text(
                    "belajar Niomic",
                    style: TextStyle(
                        height: 2.0, fontSize: 20.0, color: Colors.red),
                  ),
                  // CardIcon(),
                  ElevatedButton(
                    child: Text("Page Icon Card"),
                    onPressed: () {
                      Navigator.of(context).pushNamed("/PageIconCard");
                    },
                  ),
                  ElevatedButton(
                    child: Text("Page Number Screen"),
                    onPressed: () {
                      Navigator.of(context).pushNamed("/PageNumberScreen");
                    },
                  ),
                  Padding(padding: EdgeInsets.all(20.0)),

                  ElevatedButton(
                    child: Text("Page Web View"),
                    onPressed: () {
                      Navigator.of(context).pushNamed("/PageWebView");
                    },
                  ),

                  Image.network(
                      "https://images.unsplash.com/photo-1569226022944-7c948d846270?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=600&q=60")
                  // NumberScreen()
                  ,
                  Text("Content 1"),
                  Text("Content 2"),
                  Text("Content 3"),
                  Text("Content 4"),
                  Text("Content 5")
                ],
              ),
            ],
          ),
        ));
  }
}
