import 'package:flutter/material.dart';

class PageIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text("class Page Icon"),
    );
  }
}

class CardIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
        color: Colors.blue,
        child: Column(
          children: <Widget>[
            Icon(
              Icons.home,
              size: 60,
              color: Colors.white,
            ),
            Text(
              "Icon Home",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            ElevatedButton(
                child: Text("Home Page"),
                onPressed: () {
                  Navigator.of(context).pushNamed("/");
                }),
          ],
        ));
  }
}
