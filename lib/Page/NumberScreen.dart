import 'package:flutter/material.dart';

class NumberScreen extends StatefulWidget {
  String title;

  NumberScreen({Key? key, required this.title}) : super(key: key);

  @override
  _NumberScreenState createState() => _NumberScreenState();
}

class _NumberScreenState extends State<NumberScreen> {
  int number = 0;
  String namaUser = " ";
  String valueText = " ";

  void setName(String _value, String _value2) {
    setState(() {
      this.namaUser = _value2 + _value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Text(this.number.toString()),
            ElevatedButton(
                onPressed: () {
                  setState(() {
                    this.number += 1;
                  });
                },
                child: Text("Tambah")),
            ElevatedButton(
                onPressed: () {
                  setState(() {
                    this.number -= 1;
                  });
                },
                child: Text("kurang")),
            TextField(
              decoration: InputDecoration(hintText: "Masukan Nama Anda :"),
              onChanged: (String value) {
                setState(() {
                  this.valueText = value;
                });
              },
            ),
            ElevatedButton(
                onPressed: () {
                  setName(valueText, "Hai ");
                },
                child: Text("Submit Nama Anda")),
            Text(
              this.namaUser,
              style: TextStyle(fontSize: 30.0),
            ),
          ],
        ),
      ),
    );
  }
}
